require('dotenv').config()
const express = require('express')
const consola = require('consola')
const {
  Nuxt,
  Builder
} = require('nuxt')
const app = express()
const host = process.env.HOST || '127.0.0.1'
const port = process.env.PORT || 3000
const mongoose = require('mongoose');
const passport = require('passport');
const session = require('express-session');
const FileStore = require('session-file-store')(session);
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const authenticate = require('./authenticate');
const cors = require('cors');

app.set('port', port);

// Connect to MongoDB via Mongoose
const connect = mongoose.connect(process.env.MONGODB_URI);

connect.then(db => {
  console.log('Connected correctly to server');
}, err => {
  console.log(err);
});

// Import routes
const usersRouter = require('./routes/users')
const boardsRouter = require('./routes/boards')

// Middleware
app.use(logger('dev'));
app.use(express.json());
app.use(cors());
app.use(session({
  name: 'session-id',
  secret: '12345-67890-09876-54321',
  saveUninitialized: false,
  resave: false,
  store: new FileStore()
}));

app.use(passport.initialize());
app.use(passport.session());
app.use('/users', usersRouter);
app.use('/boards', boardsRouter);

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)

  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
