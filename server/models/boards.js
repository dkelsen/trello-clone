var mongoose = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');

var BoardSchema = new mongoose.Schema({
  boards: [{
    _id: false,
    title: String,
    keyTitle: String,
    lists: [{
      _id: false,
      enterCardTitle: Boolean,
      newCard: String,
      heading: String,
      keyHeading: String,
      renameListTitle: Boolean,
      cards: [{
        _id: false,
        title: String,
      }],
      newList: String,
    }],
  }],
  owner: {
    type: String,
    _id: false
  }
});

BoardSchema.plugin(passportLocalMongoose);

var User = mongoose.model('Board', BoardSchema);
module.exports = User;
