const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const User = require('../models/user');
const Board = require('../models/boards');

router.use(bodyParser.json());

// Save content
router.post('/save', (req, res, next) => {
  Board.findOne({
    owner: req.body.username
  }, (error, board) => {
    if (error) {
      console.log(error)
      //board does not exist
    } else if (board == null) {
      Board.create({
          boards: req.body.board,
          owner: req.body.username
        })
        .then(response => {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json({
            success: true,
            status: 'Board Saved!'
          });
        })
        .catch(err => console.log(err));
    } else {
      board.remove();
      Board.create({
          boards: req.body.board,
          owner: req.body.username
        })
        .then(response => {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json({
            success: true,
            status: 'Board Saved!'
          });
        })
    }
  })
});

router.post('/retrieve', (req, res, next) => {
  Board.findOne({
    owner: req.body.username
  }, (error, board) => {
    if (error) {
      console.log(error)
      //board does not exist
    } else if (board == null) {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json({
        success: true,
        status: 'User has no boards!'
      });
    } else {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json({
        success: true,
        status: 'Board Saved!',
        content: board
      });
    }
  })
});


module.exports = router;
