export default {
  methods: {
    saveOnServer() {
      if (this.currentUser != null) {
        // console.log('Saving...');
        fetch(process.env.baseUrl + 'boards/save', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              username: this.currentUser.username,
              board: this.boards
            })
          }).then(response => response.json())
          .then(data => console.log(data))
          .catch(error => console.log(error));
      }
    }
  }
}
