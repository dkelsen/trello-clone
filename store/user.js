export const state = () => ({
  signupDialog: false,
  loginDialog: false,
  currentUser: null,
  loggedIn: false
});

export const mutations = {
  changeSignupDialog(state) {
    state.signupDialog = !state.signupDialog
  },
  changeLoginDialog(state) {
    state.loginDialog = !state.loginDialog
  },
  changeUser(state, payload) {
    state.currentUser = payload.data
  },
  changeLoggedIn(state, payload) {
    state.loggedIn = payload
  }
}

export const actions = {
  changeSignupDialog({commit}) {
    commit('changeSignupDialog')
  },
  changeLoginDialog({commit}) {
    commit('changeLoginDialog')
  },
  changeUser({commit}, payload) {
    commit('changeUser', payload)
  },
  changeLoggedIn({commit}, payload) {
    commit('changeLoggedIn', payload)
  }
};
