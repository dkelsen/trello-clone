export const state = () => ({
  boards: []
});

export const mutations = {
  fetchBoards(state, payload) { // fetched from server
    state.boards = payload.boards
  },
  changeListHeading(state, payload) {
    state.boards[payload.boardIndex].lists[payload.listIndex].heading = payload.event;
  },
  writeCardTitle(state, payload) {
    state.boards[payload.boardIndex].lists[payload.listIndex].newCard = payload.event;
  },
  // Vue draggable
  updateList(state, payload) {
    state.boards[payload.boardIndex].lists[payload.listIndex].cards = payload.value;
  },
  createBoard(state, payload) {
    if (payload.newBoardTitle != null) {
      state.boards.push({
        title: payload.newBoardTitle,
        keyTitle: payload.newBoardTitle, // for key on front page
        lists: [],
        newList: null, // new list title
      })
    }
  },
  addList(state, payload) {
    state.boards[payload.boardIndex].lists.push({
      enterCardTitle: false,
      newCard: null,
      heading: payload.newList,
      keyHeading: payload.newList, // for key
      renameListTitle: false,
      cards: []
    });
  },
  deleteList(state, payload) {
    state.boards[payload.boardIndex].lists.splice(payload.listIndex, 1);
  },
  deleteBoard(state, payload) {
    state.boards.splice(payload.boardIndex, 1);
  },
  // toggles input for list title
  changeRenameListTitle(state, payload) {
    let list = state.boards[payload.boardIndex].lists[payload.listIndex];
    list.renameListTitle = !list.renameListTitle;
  },
  // toggles add button for cards
  enterTitle(state, payload) {
    let list = state.boards[payload.boardIndex].lists[payload.listIndex];
    list.enterCardTitle = !list.enterCardTitle;
  },
  addCard(state, payload) {
    let list = state.boards[payload.boardIndex].lists[payload.listIndex];
    if (list.newCard != null) {
      list.cards.push({ title: list.newCard });
    }
  },
  removeCard(state, payload) {
    let list = state.boards[payload.boardIndex].lists[payload.listIndex];
    list.cards.splice(payload.cardIndex, 1);
  }
}

export const actions = {
  fetchBoards({commit}, payload) {
    commit('fetchBoards', payload)
  },
  createBoard({
    commit
  }, payload) {
    commit('createBoard', payload)
  },
  addList({
    commit
  }, payload) {
    commit('addList', payload)
  },
  deleteList({commit}, payload) {
    commit('deleteList', payload)
  },
  deleteBoard({commit}, payload) {
    commit('deleteBoard', payload)
  },
  changeRenameListTitle({commit}, payload) {
    commit('changeRenameListTitle', payload)
  },
  changeListHeading({commit}, payload) {
    commit('changeListHeading', payload)
  },
  enterTitle({commit}, payload) {
    commit('enterTitle', payload)
  },
  addCard({commit}, payload) {
    commit('addCard', payload)
  },
  removeCard({commit}, payload) {
    commit('removeCard', payload)
  },
  writeCardTitle({commit}, payload) {
    commit('writeCardTitle', payload)
  }
};

export const getters = {
  getListHeading: (state) => (payload) => {
    return state.boards[payload.boardIndex].lists[payload.listIndex].heading;
  }
}
